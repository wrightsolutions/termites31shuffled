# Cons2.TERMITES31X hex input - shuffled and processed

## Shuffle process used

The script multiput0x.py exists in the following repositories and uses a onetime key of `2112312020190909`

+[sal683onetime32](https://bitbucket.org/wrightsolutions/sal683onetime32)

+[sal683onetime31](https://bitbucket.org/wrightsolutions/sal683onetime31)  ... older and less preferred version




## Input hashes plus Rowcounts and why not a round 100000 or 200000 in every case?

Each shuffled version of the hex of TERMITES21X is pre-validated to ensure entropy (usually passes) and
decimal equivalent is sufficiently sized ( >= DECMIN, and <= DECMAX )

Failures have been removed, reducing the round 100000 input to a bit less.

Directory rows100000/ contains the shuffled hex we will use as input to the hashing process


## Source as csv

Because the csv source takes up a bit more space than the results output I have not provided it in every case so
as not to bloat the repo too much

Directory rowsource/ has the csv data for some of the hashing runs.


## Output samples

Directory ver32multiput0x/ contains the results of the hashing process


## Output uniqueness - smallish sample to demonstrate lack of clashes

```
zcat termites31shuffled30001transmithex.txt.gz | cut -d',' -f7 > /tmp/termites31shuffled30001hex.txt

wc -l /tmp/termites31shuffled30001hex.txt
30001 /tmp/termites31shuffled30001hex.txt

uniq /tmp/termites31shuffled30001hex.txt | wc -l
30001
```


## Hints about rejection rates - very approximate

+ Seeing 1% rejection in a 10000 row input fits with what we might expect with the new flexible entropy implementation
+ Seeing 10% rejection suggests that the hex you are supplying has not been vetted to ensure it is stringable (on input) to base64

Our entropy mechanism, and I suppose the hashing itself, relies on a consistent base64 representation of the input ( decimal / hex )

You should pre-filter hex that is not paired or countable to a standard that supports translation to base64, or,   
use the harness_prevalidator.py script to test your row before including it in a file of hex for multiput0x.py


